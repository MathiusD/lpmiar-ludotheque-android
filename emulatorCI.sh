ANDROID_HOME=$1
EMULATOR=$2
GRADLE_OPTS=$3

if ! kvm-ok; then
    echo "GitLab Runner don't support Emulator, Tests ignored"
else
    "$ANDROID_HOME"/tools/bin/avdmanager create avd -f -n api -k "$EMULATOR" -d "pixel" -c 128M
    "$ANDROID_HOME"/tools/emulator -avd api -wipe-data -no-audio -no-window -gpu off -verbose -qemu -vnc :2
    "$ANDROID_HOME"/platform-tools/adb wait-for-device
    adb shell input keyevent 82
    ./gradlew connectedCheck ${GRADLE_OPTS}
fi
