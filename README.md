# Android-Client

## Note en cas de Fork/Reprise

La ci est prévue pour déployer à la fois sur le serveur de l'iut de nantes et mon serveur personnel.
En ce sens on a à peu près 3 000 variables de CI que je document ci après.
Dans la ci sauf si vous souhaitez déployer sur un tomcat perso je vous déconseille fortement de conserver la section de déployement `mathius` (Dans la ci et dans le pom dcp).
D'ailleurs les runners gitlab de l'univ n'ont pas accès à central vous devez donc [pluger une ci externe](https://gitlab.com/projects/new#cicd_for_external_repo) ou [mettre en place un miroir](https://gitlab.com/projects/new#import_project) (Pour le dernier choix faut juste penser à cocher la case `Mirror repository`).
Petit détail, les liens sont sur `gitlab.com` mais si vous avez une instance perso vous pouvez aussi.

### Variables CI

#### Construction

##### IUT

Pour gérer construire l'apk associés aux api de l'iut

* `API_DEVELOP_IUT` : Api de dev de l'iut
* `API_RELEASE_IUT` : Api de prod de l'iut

##### Perso

Pour gérer le déployement automatique sur un tomcat perso (Si vous l'utilisez je vous conseille de revoir le pom et la ci)

* `API_DEVELOP_MATHIUS` : Api de dev perso
* `API_RELEASE_MATHIUS` : Api de prod perso

Si vous n'envisager pas de déployer sur un serveur personnel je vous conseille de supprimer les tâches *:mathius dans la ci
