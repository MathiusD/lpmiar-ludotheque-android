package com.example.android_client;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import java.util.concurrent.ExecutionException;

public class ChoixModifEmplacement extends AppCompatActivity {

    // variables
    private EditText id_loan;
    private Button validerId;
    private JsonObject getIdLoan;
    private String url;
    private String id_loans;
    private String userToken;
    private Bundle extras;

    // initialisation
    private void initialiser(){
        id_loan = this.findViewById(R.id.id_jeu);
        url =  BuildConfig.URL_KEY;
        validerId = this.findViewById(R.id.valider_id);
        extras = getIntent().getExtras();
        userToken = extras.getString("token");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modiflieu);

        initialiser();
        validerId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id_loan.getText().toString().equals("")){
                    Toast.makeText(getBaseContext(), "Veuillez rentrer un ID", Toast.LENGTH_LONG).show();
                }else {

                    if (loan_exist()) {
                        Intent intent = new Intent(ChoixModifEmplacement.this, ChoixModifEmplacementId.class);
                        String id = id_loan.getText().toString();
                        intent.putExtra("id_jeu", id_loans);
                        intent.putExtra("token" , userToken);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(getBaseContext(), "Veuillez rentrer un ID valide", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private boolean loan_exist(){
        boolean res = true;
        try {
            getIdLoan = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"/games/"+id_loan.getText().toString())
                    .asJsonObject()
                    .get();
            try {
                id_loans = getIdLoan.getAsJsonObject().getAsJsonObject().getAsJsonPrimitive("id").getAsString();
            } catch (Exception e) {
                res = false;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            res = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
            res = false;
        }
        return res;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent(ChoixModifEmplacement.this, ConnecteActivity.class);
        intent.putExtra("token" , userToken);
        startActivity(intent);
        finish ();
        return true;
    }
}
