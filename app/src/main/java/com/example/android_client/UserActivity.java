package com.example.android_client;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android_client.tools.Emprunt;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.http.AsyncHttpClientMiddleware;
import com.koushikdutta.async.http.SimpleMiddleware;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class UserActivity extends AppCompatActivity {

    // Variables
    private List<String> userEmprunt;
    private List<String> idJeu;
    private TextView userInfo;
    private ListView listeEmprunt;
    private JsonObject getUserInfo;
    private JsonArray getUserLoans;
    private Bundle extras;
    private String user;
    protected static List spinnerList = new ArrayList();
    Spinner spinner;
    private String monChoix;
    private static String toutLesJeux = "Tout"; // Tout les états de jeux
    private static String attentePrepa = "Attente Prepa"; // Requested  - Jeu commandé
    private static String emprunté = "Emprunté"; // At Home - Jeu chez l'utilisateur
    private static String attenteRecup = "Attente Recup"; // Waiting - Jeu attente recup utilisateur
    private String url;
    private String userToken;

    // Initialisation
    private void initalisation(){
        userInfo = (TextView) this.findViewById(R.id.user_info);
        listeEmprunt = (ListView) this.findViewById(R.id.liste_emprunt);
        getUserInfo = null;
        getUserLoans = null;
        userEmprunt = new ArrayList<>();
        idJeu = new ArrayList<>();
        extras = getIntent().getExtras();
        user = extras.getString("user");
        url =  BuildConfig.URL_KEY;
        userToken = extras.getString("token");
    }

    // onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        //Récupération du Spinner
        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerList.clear();
        spinnerList.add(toutLesJeux);
        spinnerList.add(attentePrepa);
        spinnerList.add(emprunté);
        spinnerList.add(attenteRecup);
        //Passage d'un contexte et d'un fichier de présentation dans un adapter pour le spinner
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, spinnerList);
        //Présentation du spinner déroulé
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Passage de l'adapter au spinner
        spinner.setAdapter(adapter);
        initalisation();
        getUserInfo();
        getUserEmprunt();
        affichageListeJeux();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monChoix = String.valueOf(spinner.getSelectedItem());
                if (monChoix.equals(toutLesJeux)){
                    getUserEmprunt();
                }else if (monChoix.equals(attentePrepa)){
                    getUserEmpruntAttentePrepa();
                }else if (monChoix.equals(emprunté)){
                    getUserEmpruntEmprunte();
                }else if (monChoix.equals(attenteRecup)){
                    getUserAttenteRecup();
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        listeEmprunt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = listeEmprunt.getItemAtPosition(position);
                String str = (String) o;
                String  idDuJeu = idJeu.get(userEmprunt.indexOf(str));
                Intent intent = new Intent(UserActivity.this, EmpruntDetailActivity.class);
                intent.putExtra("token" , userToken);
                intent.putExtra("jeux" , idDuJeu);
                intent.putExtra("user",user);
                startActivity(intent);
                finish();
            }
        });
    }

    // récuperation des info utilisateur
    private void getUserInfo(){
        String userId = "null";
        String userFirstName = "null";
        String userLastName = "null";
        String userBirthDate = "null";
        String userAdress = "null";
        String userCity = "null";
        try {
            getUserInfo = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/users/"+user)
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();

            try {
                userId = getUserInfo.getAsJsonObject().getAsJsonPrimitive("id").getAsString();
                userFirstName = getUserInfo.getAsJsonObject().getAsJsonPrimitive("firstName").getAsString();
                userLastName = getUserInfo.getAsJsonObject().getAsJsonPrimitive("lastName").getAsString();
                userBirthDate = getUserInfo.getAsJsonObject().getAsJsonPrimitive("birthDate").getAsString();
                userAdress = getUserInfo.getAsJsonObject().getAsJsonPrimitive("address").getAsString();
                userCity = getUserInfo.getAsJsonObject().getAsJsonPrimitive("city").getAsString();
            }catch (Exception e){
                e.printStackTrace();
            }
            String texte ="\n Prenom : " + userFirstName + "\n Nom : " + userLastName + "\n id :" + userId + "\n date de naissance : " + userBirthDate + "\n Adresse : " + userAdress + ", " +userCity;

            userInfo.setText(texte);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // récuperation des emprunts utilisateur
    private void getUserEmprunt(){
        try {
            userEmprunt.clear();
            idJeu.clear();
            getUserLoans = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/users/"+user+"/loans")
                    .setHeader("token",(userToken))
                    .asJsonArray()
                    .get();

            for (JsonElement jeu : getUserLoans) {
                String game_name = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("name").getAsString();
                String game_id = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("id").getAsString();
                String etatJeu = jeu.getAsJsonObject().getAsJsonObject("state").getAsJsonPrimitive("name").getAsString();

                if(!etatJeu.equals("Rendered")){
                    idJeu.add(game_id);
                    userEmprunt.add(game_name);
                }
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        affichageListeJeux();
    }

    // récuperation des emprunts en attente utilisateur
    private void getUserEmpruntAttentePrepa(){
        try {
            userEmprunt.clear();
            idJeu.clear();
            getUserLoans = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/users/"+user+"/loans?state=Requested")
                    .setHeader("token",(userToken))
                    .asJsonArray()
                    .get();
            for (JsonElement jeu : getUserLoans) {
                String game_name = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("name").getAsString();
                String game_id = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("id").getAsString();
                idJeu.add(game_id);
                userEmprunt.add(game_name);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        affichageListeJeux();
    }

    // Jeu emprunté
    private void getUserEmpruntEmprunte(){
        try {
            userEmprunt.clear();
            idJeu.clear();
            getUserLoans = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/users/"+user+"/loans?state=At_Home")
                    .setHeader("token",(userToken))
                    .asJsonArray()
                    .get();
            for (JsonElement jeu : getUserLoans) {
                String game_name = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("name").getAsString();
                String game_id = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("id").getAsString();
                idJeu.add(game_id);
                userEmprunt.add(game_name);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        affichageListeJeux();
    }

    // Jeu En Attente De Recup
    private void getUserAttenteRecup(){
        try {
            userEmprunt.clear();
            idJeu.clear();
            getUserLoans = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/users/"+user+"/loans?state=Waiting")
                    .setHeader("token",(userToken))
                    .asJsonArray()
                    .get();
            for (JsonElement jeu : getUserLoans) {
                String game_name = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("name").getAsString();
                String game_id = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("id").getAsString();
                idJeu.add(game_id);
                userEmprunt.add(game_name);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        affichageListeJeux();
    }
    // Donnée de la Liste View
    private void affichageListeJeux(){
        ArrayAdapter<String> monAdapteur
                = new ArrayAdapter <String>( UserActivity.this ,
                android.R.layout.simple_list_item_1 , userEmprunt);
        listeEmprunt.setAdapter(monAdapteur);
    }
    // Clean de la spinnerList au retour de page
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent result = new Intent ();
            result.putExtra("token" , userToken);
            spinnerList.clear();
            finish ();
        }
        return true;
    }
}