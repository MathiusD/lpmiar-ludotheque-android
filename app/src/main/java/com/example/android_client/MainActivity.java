package com.example.android_client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static java.lang.Thread.sleep;

public class MainActivity extends AppCompatActivity {

    // variables
    private String url;
    private JsonObject getUserLoans;
    private  String urrl;
    private String test;
    private String uniqueId;
    private  WebView myWebView;
    private String provider;
    private Button connexionDiscord;
    private Button connexionGitlab;
    private Button connexionGoogle;
    private Button connexionZoom;
    private Button connexionGithub;
    private Button validerConnexion;

    // initialisation
    private void initialiser(){
        url =  BuildConfig.URL_KEY;
        getUserLoans = null;
        myWebView = ( WebView ) findViewById (R.id.webview);
        connexionDiscord = (Button) this.findViewById(R.id.connexionDiscord);
        connexionZoom = (Button) this.findViewById(R.id.connexionZoom);
        connexionGitlab = (Button) this.findViewById(R.id.connexionGitlab);
        connexionGithub = (Button) this.findViewById(R.id.connextionGithub);
        connexionGoogle = (Button) this.findViewById(R.id.connexionGoogle);
        validerConnexion = (Button) this.findViewById(R.id.valider_connexion);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialiser();

        connexionDiscord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                provider = "login/discord";
                getWebView();
            }
        });

        connexionGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                provider = "login/google";
                getWebView();
            }
        });

        connexionGitlab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                provider = "login/gitlab";
                getWebView();
            }
        });

        connexionGithub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                provider = "login/github";
                getWebView();
            }
        });

        connexionZoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                provider = "login/zoom";
                getWebView();
            }
        });

        validerConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserConnexion(uniqueId);
            }
        });

    }

    private void getWebView(){
        uniqueId = String.valueOf(UUID.randomUUID());
        myWebView.loadUrl (url+provider+"?key="+uniqueId);
        urrl= myWebView.getUrl();
        test =  url+provider;
    }


    // récuperation des emprunts utilisateur
     private void getUserConnexion(String uniqueId){
        String userToken = "null" ;
        try {
            getUserLoans = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"oauth/return/"+uniqueId)
                    .setLogging("userId", Log.DEBUG)
                    .asJsonObject()
                    .get();
            try {
                userToken = getUserLoans.getAsJsonObject().getAsJsonPrimitive("token").getAsString();
            }catch (Exception e){
                e.printStackTrace();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(MainActivity.this, ConnecteActivity.class);
        intent.putExtra("token",userToken);

        if(!userToken.equals("null")){
            startActivity(intent);
        }else{
            Toast.makeText(getBaseContext(), "Veuillez vous connecter", Toast.LENGTH_LONG).show();
        }

    }

}