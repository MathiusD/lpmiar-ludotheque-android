package com.example.android_client;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ConnecteActivity extends AppCompatActivity {

    // Variable
    private String url;
    private JsonArray get_user_loans;
    private List<String> nom_user;
    private Button gerer_les_paquets;
    Integer DETAIL_INVITE_REQUEST_CODE = 1;
    private String userToken;
    private Bundle extras;

    // Initialisation
    private void initalisation(){
        url =  BuildConfig.URL_KEY;
        get_user_loans = null;
        nom_user = new ArrayList<>();
        extras = getIntent().getExtras();
        userToken = extras.getString("token");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connecte);

        Button gerer_les_emprunts = (Button) this.findViewById(R.id.gerer_les_emprunts);
        gerer_les_paquets = (Button) this.findViewById(R.id.gerer_les_paquets);
        Button deconnection = (Button) this.findViewById(R.id.deconnection);
        Button gerer_les_retours = (Button) this.findViewById(R.id.gerer_les_retours);
        Button gererLesEnvoi = (Button) this.findViewById(R.id.recuperationpaquet);
        Button modifEmplacementJeu = (Button) this.findViewById(R.id.modif_emplacement_jeux);

        initalisation();
        getUserEmpruntAttentePrepa();
        getEtateButton();

        modifEmplacementJeu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnecteActivity.this, ChoixModifEmplacement.class);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish();
            }
        });


        gerer_les_emprunts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnecteActivity.this, EmpruntActivity.class);
                String choix = "emprunt";
                intent.putExtra("token" , userToken);
                intent.putExtra("choix" , choix);
                startActivity(intent);
                finish();
            }
        });

        gererLesEnvoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnecteActivity.this, PaquetRecup.class);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish();
            }
        });

        gerer_les_paquets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(ConnecteActivity.this, GestionPaquet.class);
                    intent.putExtra("token" , userToken);
                    startActivity(intent);
                    finish();
                }
        });

        gerer_les_retours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnecteActivity.this, EnregistrementRetour.class);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish();
            }
        });

        deconnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConnecteActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        
    }

    private void getEtateButton(){
        if(nom_user.isEmpty()){
            gerer_les_paquets.setEnabled(false);
            gerer_les_paquets.setText("Pas de Paquet à traiter");
            gerer_les_paquets.setTextColor(Color.BLACK);
        }else{
            gerer_les_paquets.setClickable(true);
        }
    }


    // récuperation des emprunts en attente
    private void getUserEmpruntAttentePrepa(){
        try {
            nom_user.clear();
            get_user_loans = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/loans?state=requested")
                    .noCache()
                    .setHeader("token",(userToken))
                    .asJsonArray()
                    .get();
            for (JsonElement user : get_user_loans) {
                String lastName_user = user.getAsJsonObject().getAsJsonPrimitive("lastName").getAsString();
                String firstName_user = user.getAsJsonObject().getAsJsonPrimitive("firstName").getAsString();
                nom_user.add(lastName_user + " " + firstName_user);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Toast.makeText(getBaseContext(), "Veuillez vous deconnectez", Toast.LENGTH_LONG).show();
        return true;
    }

}