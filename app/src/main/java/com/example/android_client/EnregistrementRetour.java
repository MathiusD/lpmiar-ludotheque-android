package com.example.android_client;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.concurrent.ExecutionException;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.koushikdutta.ion.Ion;

import java.util.concurrent.ExecutionException;

public class EnregistrementRetour extends AppCompatActivity {

    // variables
    private EditText id_loan;
    private Button valider_id;
    private Button scanner_code;
    private JsonObject get_id_loan;
    private String url;
    private String id_loans;
    private String idJeu;
    private String userToken;
    private Bundle extras;
    private Boolean passer;

    // initialisation
    private void initialiser(){
        id_loan = this.findViewById(R.id.id_jeu);
        url =  BuildConfig.URL_KEY;
        valider_id = this.findViewById(R.id.valider_id);
        scanner_code = this.findViewById(R.id.scanner_code);
        idJeu=null;
        extras = getIntent().getExtras();
        userToken = extras.getString("token");
        passer = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enregistrement_retour);

        initialiser();
        valider_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (id_loan.getText().toString().equals("")){
                    Toast.makeText(getBaseContext(), "Veuillez rentrer un ID", Toast.LENGTH_LONG).show();
                }else {

                    if (loan_exist()) {
                        Intent intent = new Intent(EnregistrementRetour.this, GestionRetour.class);
                        String id = id_loan.getText().toString();
                        intent.putExtra("token" , userToken);
                        intent.putExtra("id_loans", id_loans);
                        intent.putExtra("id_jeu", id_loan.getText().toString());
                        startActivity(intent);
                    }else{
                        Toast.makeText(getBaseContext(), "Veuillez rentrer un ID valide", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        scanner_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scannerQRcode();
            }
        });
    }

    private boolean loan_exist(){
        boolean res = true;
        try {
            get_id_loan = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"games/"+id_loan.getText().toString()+"/loanRelated")
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();
            try {
                id_loans = get_id_loan.getAsJsonObject().getAsJsonObject("currentLoanRelated").getAsJsonPrimitive("id").getAsString();
            } catch (Exception e) {
                res = false;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            res = false;
        } catch (InterruptedException e) {
            e.printStackTrace();
            res = false;
        }
        return res;
    }

    // fonction scanner RQcode
    private void scannerQRcode(){
        IntentIntegrator integrator =new IntentIntegrator(this);
        integrator.setCaptureActivity(QrCodeScan.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning code");
        integrator.initiateScan();
    }
    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data ) {
        IntentResult urlGetIdLoan = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(urlGetIdLoan != null){
            if(urlGetIdLoan.getContents() !=null){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Le jeu est il rendu en mauvais état ?");
                builder.setPositiveButton("oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        validationRetourQrCode(urlGetIdLoan.getContents());
                        MauvaisEtat();
                    }
                }).setNegativeButton("non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        validationRetourQrCode(urlGetIdLoan.getContents());

                    }
                });
                AlertDialog popUp = builder.create();
                popUp.show();
            }
            else{
                Toast.makeText(this,"Erreur détection qrCode",Toast.LENGTH_LONG).show();
            }
        }else{
            super.onActivityResult(requestCode,resultCode,data);
        }
    }
    // methode post
    private void validationRetourQrCode(String url_get_id_loan){
        String id_loan = null;
        try {
            get_id_loan = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url_get_id_loan)
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();
            try {
                idJeu = get_id_loan.getAsJsonObject().getAsJsonObject("gameRelated").getAsJsonPrimitive("id").getAsString();
                id_loan = get_id_loan.getAsJsonObject().getAsJsonObject("currentLoanRelated").getAsJsonPrimitive("id").getAsString();
                passer = true;
            } catch (Exception  e) {
                e.printStackTrace();
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(passer == true) {
            JsonObject json = new JsonObject();
            json.addProperty("state", "rendered");
            Ion.with(getApplicationContext())
                    .load(url + "loans/" + id_loan)
                    .setHeader("token", (userToken))
                    .setJsonObjectBody(json)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                            } else if (result != null) {
                                try {
                                    String error = result.getAsJsonObject().getAsJsonPrimitive("raisonProvided").getAsString();
                                    Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
                                } catch (Exception a) {
                                    a.printStackTrace();
                                    Toast.makeText(getBaseContext(), "Retour enregisté !", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });
        }else {
            Toast.makeText(getBaseContext(), "Ce QR code ne correspond à aucun emprunt en cours !", Toast.LENGTH_LONG).show();
        }
    }

    private void MauvaisEtat(){
        JsonObject jsonState = new JsonObject();
        JsonObject jsonEtat = new JsonObject();
        jsonState.addProperty("name", "Broken");
        jsonState.addProperty("isAvailable", false);
        jsonState.addProperty("note", "This game is broken");
        jsonEtat.add("state", jsonState);
        Ion.with(getApplicationContext())
                .load(url+"games/"+idJeu)
                .setHeader("token",(userToken))
                .noCache()
                .setLogging("log", Log.DEBUG)
                .setJsonObjectBody(jsonEtat)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                        } else if (result != null){
                            try {
                                String error = result.getAsJsonObject().getAsJsonPrimitive("message").getAsString();
                                Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
                            }catch (Exception a){
                                a.printStackTrace();
                                Toast.makeText(getBaseContext(), "Mauvais état du jeu signalé !", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent(EnregistrementRetour.this, ConnecteActivity.class);
        intent.putExtra("token" , userToken);
        startActivity(intent);
        finish ();
        return true;
    }

}