package com.example.android_client;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.concurrent.ExecutionException;

public class PaquetRecupId  extends AppCompatActivity {


    // variables
    private TextView infoJeu;
    private Button valider;
    private Bundle extras;
    private String idJeu;
    private String urlPost;
    private String url;
    private String userToken;

    // initialisation
    private void initialiser() {
        infoJeu = this.findViewById(R.id.info_jeu);
        valider = this.findViewById(R.id.valider_retour);
        extras = getIntent().getExtras();
        idJeu = extras.getString("id_jeu");
        userToken = extras.getString("token");
        url =  BuildConfig.URL_KEY;
    }

    // onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paquetrecupid);

        initialiser();
        get_loan_info();

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlPost = url+"library/loans/"+idJeu;
                post(urlPost,"at_home");
                Intent intent = new Intent(PaquetRecupId.this, PaquetRecup.class);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish();
            }
        });
    }

    //récupérer les info de l'emprunt
    private void get_loan_info(){
        try {
            JsonObject loan_info = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/loans/"+idJeu)
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();
            String gameName = loan_info.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("name").getAsString();
            String gameId = loan_info.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("id").getAsString();
            String gameEmpruntDebut = loan_info.getAsJsonObject().getAsJsonPrimitive("startedAt").getAsString();
            String gameEmpruntFin = loan_info.getAsJsonObject().getAsJsonPrimitive("endAt").getAsString();
            infoJeu.setText("Nom du jeu : "+gameName+"\n Id du jeu : "+gameId+"\n date d'emprunt : "+gameEmpruntDebut+"\n date de retour estimée : "+gameEmpruntFin);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // methode post
    private void post(String url, String state){
        JsonObject json = new JsonObject();
        json.addProperty("state", state);

        // signale le retour du jeu
        Ion.with(getApplicationContext())
                .load(url)
                .setHeader("token",(userToken))
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                        } else if (result != null){
                            try {
                                String error = result.getAsJsonObject().getAsJsonPrimitive("raisonProvided").getAsString();
                                Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
                            }catch (Exception a){
                                a.printStackTrace();
                                Toast.makeText(getBaseContext(), "Le jeux à bien été transmis !", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent(PaquetRecupId.this, PaquetRecup.class);
        intent.putExtra("token" , userToken);
        startActivity(intent);
        finish ();
        return true;
    }
}
