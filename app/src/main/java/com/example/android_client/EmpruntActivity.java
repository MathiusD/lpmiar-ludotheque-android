package com.example.android_client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android_client.tools.Utilisateur;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class EmpruntActivity extends AppCompatActivity {

    // variables
    private List<Utilisateur> userList;
    private List<Utilisateur> userRecherche;
    private Bundle extras;
    private String choix;
    private ListView listeUser ;
    private TextInputEditText nomRecherche;
    private Button rechercher ;
    private String url;
    private String userToken;

    //initialisation
    private void initialiser(){
        listeUser = this.findViewById(R.id.liste_user);
        userRecherche = new ArrayList<>();
        nomRecherche = this.findViewById(R.id.nom_recherche);
        rechercher = this.findViewById(R.id.rechercher);
        extras = getIntent().getExtras();
        choix = extras.getString("choix");
        userToken = extras.getString("token");
        userList = new ArrayList<>();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emprunt);
        url =  BuildConfig.URL_KEY;
        initialiser();
        getUserList();
        afficherList();

    }

    // recuperation de la liste des utilisateurs
    private void getUserList(){
        JsonArray get = null;
        String userId = "null";
        String userLastName = "null";
        String userFirstName = "null";
        String userBirthDate = "null";
        try {
            get = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/users?schema=fullUser")
                    .setHeader("token",(userToken))
                    .asJsonArray()
                    .get();
            for (JsonElement user : get) {
                try {
                    userId = user.getAsJsonObject().getAsJsonPrimitive("id").getAsString();
                    userLastName = user.getAsJsonObject().getAsJsonPrimitive("lastName").getAsString();
                    userFirstName = user.getAsJsonObject().getAsJsonPrimitive("firstName").getAsString();
                    userBirthDate = user.getAsJsonObject().getAsJsonPrimitive("birthDate").getAsString();
                }catch (Exception a){
                    a.printStackTrace();
                }
                userList.add(new Utilisateur(userId,userFirstName,userLastName,userBirthDate));
                userRecherche.add(new Utilisateur(userId,userFirstName,userLastName,userBirthDate));

            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // affichage de la liste
    private void afficherList(){
        //Collections.sort(userList);
        listeUser.setAdapter(new UtilisateurListAdapter(findViewById(android.R.id.content).getRootView().getContext(), userList));
        listeUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Utilisateur o = (Utilisateur) listeUser.getItemAtPosition(position);
                String str = (String) o.getId();
                Intent intent = new Intent(EmpruntActivity.this, UserActivity.class);
                intent.putExtra("user" , str);
                intent.putExtra("token" , userToken);
                startActivity(intent);
            }
        });

        rechercher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userRecherche.clear();
                Integer i = 0;
                for (Utilisateur u : userList) {
                    i=i+1;
                    if (u.getFirstName().toLowerCase().contains(nomRecherche.getText().toString().toLowerCase()) || u.getLastName().toLowerCase().contains(nomRecherche.getText().toString().toLowerCase())){
                        userRecherche.add(u);
                    }
                }
                listeUser.setAdapter(new UtilisateurListAdapter(findViewById(android.R.id.content).getRootView().getContext(), userRecherche));
            }
        });
    }

    // adapteur personnalisé
    private class UtilisateurListAdapter extends ArrayAdapter<Utilisateur> {

        public UtilisateurListAdapter(Context context, List<Utilisateur> utilisateurs) {


            super(context,R.layout.adapteur_liste_utilisateurs, utilisateurs);
        }

        public View getView(final int position, View convertView, ViewGroup parent){
            View view;
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.adapteur_liste_utilisateurs,parent,false);

            TextView nom = view.findViewById(R.id.firstName);
            TextView prenom = view.findViewById(R.id.lastName);
            TextView id = view.findViewById(R.id.id);

            Utilisateur utilisateur = userRecherche.get(position);
            nom.setText(utilisateur.getFirstName());
            prenom.setText(utilisateur.getLastName());
            id.setText(utilisateur.getId());
            return view;
        }
    }
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent(EmpruntActivity.this, ConnecteActivity.class);
        intent.putExtra("token" , userToken);
        startActivity(intent);
        finish ();
        return true;
    }
}