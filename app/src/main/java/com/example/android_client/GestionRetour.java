package com.example.android_client;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class GestionRetour extends AppCompatActivity {

    // variables
    private TextView infoJeu;
    private CheckBox mauvaisEtat;
    private Button valider;
    private Button annuler;
    private Bundle extras;
    private String idLoans;
    private String urlPost;
    private String idJeu;
    private String url;
    private String userToken;

    // initialisation
    private void initialiser() {
        infoJeu = this.findViewById(R.id.info_jeu);
        mauvaisEtat = this.findViewById(R.id.mauvais_etat);
        valider = this.findViewById(R.id.valider_retour);
        annuler = this.findViewById(R.id.annuler);
        extras = getIntent().getExtras();
        idLoans = extras.getString("id_loans");
        idJeu = extras.getString("id_jeu");
        url =  BuildConfig.URL_KEY;
        userToken = extras.getString("token");
    }

    // onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_retour);

        initialiser();
        get_loan_info();

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlPost = url+"library/loans/"+idLoans;
                try {
                    post(urlPost,"RENDERED");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(GestionRetour.this, EnregistrementRetour.class);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish();
            }
        });

        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GestionRetour.this, EnregistrementRetour.class);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish();
            }
        });
    }

    //récupérer les info de l'emprunt
    private void get_loan_info(){
        try {
            JsonObject loan_info = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/loans/"+idLoans)
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();
                String game_name = loan_info.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("name").getAsString();
                String game_id = loan_info.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("id").getAsString();
                String game_emprunt_debut = loan_info.getAsJsonObject().getAsJsonPrimitive("startedAt").getAsString();
                String game_emprunt_fin = loan_info.getAsJsonObject().getAsJsonPrimitive("endAt").getAsString();
             infoJeu.setText("Nom du jeu : "+game_name+"\n Id du jeu : "+game_id+"\n date d'emprunt : "+game_emprunt_debut+"\n date de retour estimée : "+game_emprunt_fin);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // methode post
    private void post(String urlPost, String state) throws JSONException {
        JsonObject json = new JsonObject();
        JsonObject jsonState = new JsonObject();
        json.addProperty("state", state);
        JsonObject jsonEtat = new JsonObject();

        // signale le retour du jeu
        Ion.with(getApplicationContext())
                .load(urlPost)
                .setHeader("token",(userToken))
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                        } else if (result != null){
                            try {
                                String error = result.getAsJsonObject().getAsJsonPrimitive("raisonProvided").getAsString();
                                Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
                            }catch (Exception a){
                                a.printStackTrace();
                                Toast.makeText(getBaseContext(), "Retour enregisté !", Toast.LENGTH_LONG).show();

                                // signale le retour du jeu en mauvais état
                                if (mauvaisEtat.isChecked()){
                                    jsonState.addProperty("name", "Broken");
                                    jsonState.addProperty("isAvailable", false);
                                    jsonState.addProperty("note", "This game is broken");
                                    jsonEtat.add("state", jsonState);
                                    Ion.with(getApplicationContext())
                                            .load(url+"games/"+idJeu)
                                            .setHeader("token",("abcEFG145"))
                                            .noCache()
                                            .setLogging("log", Log.DEBUG)
                                            .setJsonObjectBody(jsonEtat)
                                            .asJsonObject()
                                            .setCallback(new FutureCallback<JsonObject>() {
                                                @Override
                                                public void onCompleted(Exception e, JsonObject result) {
                                                    if (e != null) {
                                                        Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                                                    } else if (result != null){
                                                        try {
                                                            String error = result.getAsJsonObject().getAsJsonPrimitive("message").getAsString();
                                                            Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
                                                        }catch (Exception a){
                                                            Toast.makeText(getBaseContext(), "Mauvais état du jeu signalé !", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                }
                                            });
                                }
                            }
                        }
                    }
                });


    }

    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent(GestionRetour.this, EnregistrementRetour.class);
        intent.putExtra("token" , userToken);
        startActivity(intent);
        finish ();
        return true;
    }
}
