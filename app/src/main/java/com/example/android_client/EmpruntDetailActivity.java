package com.example.android_client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class EmpruntDetailActivity extends AppCompatActivity {

    // variables
    private String Jeux;
    private JsonObject getJeuInfo;
    private TextView infoEmprunt;
    private TextView infoLocalisation;
    private TextView infoTypeJeu;
    private TextView infoIdeJeu;
    private String jeuxName;
    private String jeuxLocation;
    private String jeuxType;
    private String etatJeu;
    private String idPret;
    private String url;
    private String idUser;
    private Button enregistrerEtatJeu;
    private Intent intent;
    private String urlPost;
    private String reponseEtat;
    private String userToken;
    private Bundle extras;

    // Initialisation
    private void initialiser(){
        jeuxType = "non spécifié";
        etatJeu = "null";
        jeuxLocation = "null";
        jeuxName = "null";
        idPret=null;
        getJeuInfo = null;
        intent = getIntent();
        idUser = intent.getStringExtra("user");
        Jeux = intent.getStringExtra("jeux");
        infoEmprunt = (TextView) this.findViewById(R.id.info_emprunt);
        infoLocalisation = (TextView) this.findViewById(R.id.info_localisation);
        infoTypeJeu =  (TextView) this.findViewById(R.id.info_type_jeu);
        enregistrerEtatJeu = (Button) this.findViewById(R.id.enregistrer_retour);
        infoIdeJeu =  (TextView) this.findViewById(R.id.id_jeu);
        url =  BuildConfig.URL_KEY;
        infoIdeJeu.setText("Id du jeu : "+ Jeux);
        extras = getIntent().getExtras();
        userToken = extras.getString("token");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emprunt_detail);
        initialiser();
        getJeuInfo();

        urlPost = url+"loans/"+ idPret;
        if(etatJeu.equals("Requested")){
            enregistrerEtatJeu.setText("Valider Jeu prêt");
        }else if (etatJeu.equals("Waiting")){
            enregistrerEtatJeu.setText("Valider Jeu Recupéré");
        }else if(etatJeu.equals("At Home")){
            enregistrerEtatJeu.setText("Valider Jeu Rendu");
        }

        enregistrerEtatJeu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etatJeu.equals("Requested")){
                    reponseEtat= "Attente de récupération";
                    post(urlPost,"Waiting");
                }else if (etatJeu.equals("Waiting")){
                    reponseEtat= "Recupéré";
                    post(urlPost,"at_home");
                }else if(etatJeu.equals("At Home")){
                    reponseEtat= "Rendu";
                    post(urlPost,"rendered");
                }
                Intent intent = new Intent(EmpruntDetailActivity.this, UserActivity.class);
                intent.putExtra("user" , idUser);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish();
            }
        });
    }

    // méthode Post changer l'état d'un jeux
    private void post(String url, String state){
        JsonObject json = new JsonObject();
        json.addProperty("state",state);
        Ion.with(getApplicationContext())
                .load(url)
                .setHeader("token",(userToken))
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if(e != null ){
                            Toast.makeText(getBaseContext() ,"Data" +e.getStackTrace() , Toast.LENGTH_LONG).show();
                        }else if (result != null){
                            try {
                                String error = result.getAsJsonObject().getAsJsonPrimitive("raisonProvided").getAsString();
                                Toast.makeText(getBaseContext() , error , Toast.LENGTH_LONG).show();
                            }catch (Exception a){
                                a.printStackTrace();
                                Toast.makeText(getBaseContext() , "Jeux "+ reponseEtat , Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }

    // récuperation des info du jeux
    private void getJeuInfo(){
        try {
            getJeuInfo = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"games/"+Jeux+"/loanRelated?schema=fullGame")
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();
            try {
                jeuxLocation = getJeuInfo.getAsJsonObject().getAsJsonObject("gameRelated").getAsJsonPrimitive("location").getAsString();
                infoLocalisation.setText("Localisation du Jeu : " + jeuxLocation);
            }catch (Exception e){
                infoLocalisation.setText("Localisation du Jeu non spécifié");
               e.printStackTrace();
            }
            try {
                jeuxType = getJeuInfo.getAsJsonObject().getAsJsonObject("gameRelated").getAsJsonPrimitive("type").getAsString();
            }catch (Exception a){
            a.printStackTrace();
        }
            try {
                jeuxName = getJeuInfo.getAsJsonObject().getAsJsonObject("gameRelated").getAsJsonPrimitive("name").getAsString();
                etatJeu = getJeuInfo.getAsJsonObject().getAsJsonObject("currentLoanRelated").getAsJsonObject("state").getAsJsonPrimitive("name").getAsString();
                idPret = getJeuInfo.getAsJsonObject().getAsJsonObject("currentLoanRelated").getAsJsonPrimitive("id").getAsString();
            }catch (Exception a){
                a.printStackTrace();
            }

            infoEmprunt.setText("Nom du Jeu : " + jeuxName);
            infoTypeJeu.setText("Type du Jeu : " + jeuxType);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Clean de la spinnerList au retour de page
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(EmpruntDetailActivity.this, UserActivity.class);
            intent.putExtra("user" , idUser);
            intent.putExtra("token" , userToken);
            startActivity(intent);
            finish();
        }
        return true;
    }
}

