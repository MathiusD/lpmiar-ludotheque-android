package com.example.android_client.tools;

public class Emprunt {
    String emprunt_id;
    String game_id;
    String game_name;
    String game_pos;
    String state;

    public Emprunt(String loanId, String gameId, String name, String pos, String state){
        this.emprunt_id = loanId;
        this.game_id = gameId;
        this.game_name = name;
        this.game_pos = pos;
        this.state = state;
    }

    public void setEmprunt_id(String emprunt_id) { this.emprunt_id = emprunt_id; }

    public void setGame_id(String game_id) { this.game_id = game_id; }

    public void setGame_name(String game_name) { this.game_name = game_name; }

    public void setGame_pos(String game_pos) { this.game_pos = game_pos; }

    public void setState(String state) { this.state = state; }

    public String getEmprunt_id() { return emprunt_id; }

    public String getGame_id() { return game_id; }

    public String getGame_name() { return game_name; }

    public String getGame_pos() { return game_pos; }

    public String getState() { return state; }
}
