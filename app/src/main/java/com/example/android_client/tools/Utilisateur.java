package com.example.android_client.tools;

public class Utilisateur {

    private String id;
    private String firstName;
    private String lastName;
    private String birthDate;

    public Utilisateur(String id, String firstName, String lastName, String birthDate){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    public String getBirthDate() { return birthDate; }

    public String getFirstName() { return firstName; }

    public String getId() { return id; }

    public String getLastName() { return lastName; }

    public void setBirthDate(String birthDate) { this.birthDate = birthDate; }

    public void setFirstName(String firstName) { this.firstName = firstName; }

    public void setId(String id) { this.id = id; }

    public void setLastName(String lastName) { this.lastName = lastName; }
}
