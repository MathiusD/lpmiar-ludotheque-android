package com.example.android_client;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GestionPaquetParClient  extends AppCompatActivity {

    // Variables
    private ListView listePaquetPreparation;
    private String url;
    private List<String> user_attente_preparation_paquet;
    private JsonArray get_user_loans;
    private JsonObject get_user_name;
    private List<String> id_jeu;
    private List<String> name_jeu;
    private List<String> location_jeu;
    private String recupIdUser;
    private List<String> name_et_location;
    private TextView info_user;
    private ArrayList<String> selectedItems;
    private String urlPost;
    private List<String> selectedId;
    private String nom_user;
    private String userToken;
    private Bundle extras;

    // Initialisation
    private void initalisation(){
        listePaquetPreparation = (ListView) this.findViewById(R.id.liste_emprunt);
        user_attente_preparation_paquet = new ArrayList<>();
        get_user_loans = null;
        get_user_name = null;
        url =  BuildConfig.URL_KEY;
        id_jeu = new ArrayList<>();
        name_jeu = new ArrayList<>();
        location_jeu = new ArrayList<>();
        name_et_location = new ArrayList<>();
        selectedItems = new ArrayList<>();
        selectedId = new ArrayList<>();
        extras = getIntent().getExtras();
        userToken = extras.getString("token");

    }
    // onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestionpaquetclient);
        info_user = (TextView) this.findViewById(R.id.jeux_à_préparer);
        Intent intent = getIntent();
        recupIdUser = intent.getStringExtra("id_client");
        Button mettre_attente_recuperation = (Button) this.findViewById(R.id.button_waiting);
        initalisation();
        getUserName();
        getUserEmpruntAttentePrepa();

        info_user.setText("Liste des Jeux à Préparer Pour : " + nom_user);

        mettre_attente_recuperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String items="";
                String jeuxId="";
                for(String item:selectedItems){
                    items+="-"+ item +  "\n";
                }

                for (int i=0; i<selectedId.size(); i++ ){
                    urlPost = url+"loans/"+ selectedId.get(i);
                    post(urlPost,"Waiting");
                }
                getUserEmpruntAttentePrepa();
                selectedItems.clear();
                selectedId.clear();
            }
        });

        listePaquetPreparation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String selectedItem= ((TextView)view).getText().toString();
                     Long o = listePaquetPreparation.getItemIdAtPosition(position);
                     int test = o.intValue();
                     String userId = id_jeu.get(test);
                     if(selectedId.contains(userId)){
                         selectedId.remove(userId);
                        }else{
                         selectedId.add(userId);
                       }
                    if(selectedItems.contains(selectedItem)){
                        selectedItems.remove(selectedItem);
                    }else{
                        selectedItems.add(selectedItem);
                    }
            }
        });
    }

    // méthode Post changer l'état d'un jeux
    private void post(String url, String state){
        JsonObject json = new JsonObject();
        json.addProperty("state",state);
        Ion.with(getApplicationContext())
                .load(url)
                .setHeader("token",(userToken))
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if(e != null ){
                            Toast.makeText(getBaseContext() ,"Data" +e.getStackTrace() , Toast.LENGTH_SHORT).show();
                        }else if (result != null){
                            try {
                                String error = result.getAsJsonObject().getAsJsonPrimitive("raisonProvided").getAsString();
                                Toast.makeText(getBaseContext() , error , Toast.LENGTH_SHORT).show();
                            }catch (Exception a){
                                a.printStackTrace();
                                Toast.makeText(getBaseContext() , "Jeux en attente de récupération" , Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

    // Récuperation des emprunts en attente utilisateur
    private void getUserEmpruntAttentePrepa(){
        String game_name = "null";
        String game_id = "null";
        String game_location = "null";
        String name_user = "null";
        try {
            location_jeu.clear();
            name_et_location.clear();
            name_jeu.clear();
            id_jeu.clear();
            get_user_loans = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/users/"+recupIdUser+"/loans?state=Requested&gameSchema=fullGame")
                    .setHeader("token",(userToken))
                    .asJsonArray()
                    .get();
            for (JsonElement jeu : get_user_loans) {
                try {
                    game_name = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("name").getAsString();
                    game_id = jeu.getAsJsonObject().getAsJsonPrimitive("id").getAsString();
                    try {
                        game_location = jeu.getAsJsonObject().getAsJsonObject("game").getAsJsonPrimitive("location").getAsString();
                    }catch (Exception a){
                        game_location = " location inconnu ";
                        a.printStackTrace();
                    }
                    id_jeu.add(game_id);
                    name_jeu.add(game_name);
                    name_et_location.add( game_name + " " + game_location);
                }catch (Exception a){
                    a.printStackTrace();
                }
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        affichageListeJeux();
    }

    // récuperation du nom du client
    private void getUserName(){
        try {

            System.out.println("nomjoueur"+recupIdUser);
            get_user_name = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"users/"+recupIdUser)
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();

                String lastName_user = get_user_name.getAsJsonObject().getAsJsonPrimitive("lastName").getAsString();
                String firstName_user = get_user_name.getAsJsonObject().getAsJsonPrimitive("firstName").getAsString();

                nom_user=lastName_user + " " + firstName_user;

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Donnée de la Liste View
    private void affichageListeJeux(){
        listePaquetPreparation.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        ArrayAdapter<String> monAdapteur
                = new ArrayAdapter <String>( GestionPaquetParClient.this ,
                android.R.layout.simple_list_item_multiple_choice ,name_et_location);
        listePaquetPreparation.setAdapter(monAdapteur);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent(GestionPaquetParClient.this, GestionPaquet.class);
        intent.putExtra("token" , userToken);
        startActivity(intent);
        finish ();
        return true;
    }
}
