package com.example.android_client;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class GestionPaquet extends AppCompatActivity {

    // Variables
    private ListView listePaquetPreparation;
    private String url;
    private JsonArray get_user_loans;
    private List<String> id_user;
    private List<String> nom_user;
    private String userToken;
    private Bundle extras;

    // Initialisation
    private void initalisation(){
        listePaquetPreparation = (ListView) this.findViewById(R.id.liste_emprunt);
        get_user_loans = null;
        url =  BuildConfig.URL_KEY;
        id_user = new ArrayList<>();
        nom_user = new ArrayList<>();
        extras = getIntent().getExtras();
        userToken = extras.getString("token");
    }
    // onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestionpaquet);
        initalisation();
        getUserEmpruntAttentePrepa();

        listePaquetPreparation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = listePaquetPreparation.getItemAtPosition(position);
                String str = (String) o;
                String userId = id_user.get(nom_user.indexOf(str));
                Intent intent = new Intent(GestionPaquet.this, GestionPaquetParClient.class);
                intent.putExtra("id_client" , userId);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish ();
            }
        });
    }
    // récuperation des emprunts en attente
    private void getUserEmpruntAttentePrepa(){
        try {
            id_user.clear();
            nom_user.clear();
            get_user_loans = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"library/loans?state=requested")
                    .setHeader("token",(userToken))
                    .asJsonArray()
                    .get();
            for (JsonElement user : get_user_loans) {
                String lastName_user = user.getAsJsonObject().getAsJsonPrimitive("lastName").getAsString();
                String firstName_user = user.getAsJsonObject().getAsJsonPrimitive("firstName").getAsString();
                String identifiant_user = user.getAsJsonObject().getAsJsonPrimitive("id").getAsString();
                nom_user.add(lastName_user + " " + firstName_user);
                id_user.add(identifiant_user);
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        affichageListeJeux();
    }

    // Donnée de la Liste View
    private void affichageListeJeux(){
        ArrayAdapter<String> monAdapteur
                = new ArrayAdapter <String>( GestionPaquet.this ,
                android.R.layout.simple_list_item_1 ,nom_user);
        listePaquetPreparation.setAdapter(monAdapteur);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent(GestionPaquet.this, ConnecteActivity.class);
        intent.putExtra("token" , userToken);
        startActivity(intent);
        finish ();
        return true;
    }
}
