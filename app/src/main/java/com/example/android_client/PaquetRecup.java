package com.example.android_client;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.concurrent.ExecutionException;

public class PaquetRecup extends AppCompatActivity {

    // variables
    private Button scannerCode;
    private JsonObject getIdLoan;
    private String url;
    private Button validerId;
    private EditText idLoan;
    private String idLoans;
    private String userToken;
    private Bundle extras;
    private Boolean passer;


    // initialisation
    private void initialiser(){
        url =  BuildConfig.URL_KEY;
        validerId = this.findViewById(R.id.valider_id);
        idLoan = this.findViewById(R.id.id_jeu);
        extras = getIntent().getExtras();
        userToken = extras.getString("token");
        passer = false;
    }

    // onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paquetrecup);
        initialiser();
        scannerCode = this.findViewById(R.id.scaner_code);

        validerId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (idLoan.getText().toString().equals("")){
                    Toast.makeText(getBaseContext(), "Veuillez rentrer un ID", Toast.LENGTH_LONG).show();
                }else {
                    if (loanExist()) {
                        Intent intent = new Intent(PaquetRecup.this, PaquetRecupId.class);
                        String id = idLoan.getText().toString();
                        intent.putExtra("id_jeu", idLoans);
                        intent.putExtra("token" , userToken);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(getBaseContext(), "Veuillez rentrer un ID valide", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });



        scannerCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scannerQRcode();
            }
        });
    }

    private boolean loanExist(){
        boolean res = true;
        try {
            getIdLoan = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"/games/"+idLoan.getText().toString()+"/loanRelated")
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();
            try {
                idLoans = getIdLoan.getAsJsonObject().getAsJsonObject("currentLoanRelated").getAsJsonPrimitive("id").getAsString();
            } catch (Exception e) {
                res = false;
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
            res = false;

        } catch (InterruptedException e) {
            e.printStackTrace();
            res = false;

        }
        return res;
    }

    // fonction scanner RQcode
    private void scannerQRcode(){
        IntentIntegrator integrator =new IntentIntegrator(this);
        integrator.setCaptureActivity(QrCodeScan.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning code");
        integrator.initiateScan();
    }
    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data ) {
        IntentResult url_get_id_loan = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(url_get_id_loan != null){
            if(url_get_id_loan.getContents() !=null){
                validationRetourQrCode(url_get_id_loan.getContents());
            }
            else{
                Toast.makeText(this,"Erreur détection qrCode",Toast.LENGTH_LONG).show();
            }
        }else{
            super.onActivityResult(requestCode,resultCode,data);
        }
    }

    // methode post
    private void validationRetourQrCode(String urlGetIdLoan){
        String idLoan = "null";
        try {
            getIdLoan = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(urlGetIdLoan)
                    .setHeader("token",(userToken))
                    .asJsonObject()
                    .get();
            try {
                idLoan = getIdLoan.getAsJsonObject().getAsJsonObject("currentLoanRelated").getAsJsonPrimitive("id").getAsString();
                passer = true;
            }catch (Exception  e) {
            e.printStackTrace();
        }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (passer == true){
            JsonObject json = new JsonObject();
            json.addProperty("state", "at_home");
            Ion.with(getApplicationContext())
                    .load(url+"loans/"+idLoan)
                    .setHeader("token",(userToken))
                    .setJsonObjectBody(json)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            if (e != null) {
                                Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                            } else if (result != null){
                                try {
                                    String error = result.getAsJsonObject().getAsJsonPrimitive("raisonProvided").getAsString();
                                    Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
                                }catch (Exception a){
                                    a.printStackTrace();
                                    Toast.makeText(getBaseContext(), "jeu bien récupéré !", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });
        }else{
            Toast.makeText(getBaseContext(), "Ce QR code ne correspond à aucun emprunt en cours !", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent(PaquetRecup.this, ConnecteActivity.class);
        intent.putExtra("token" , userToken);
        startActivity(intent);
        finish ();
        return true;
    }

}
