package com.example.android_client;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.concurrent.ExecutionException;

public class ChoixModifEmplacementId  extends AppCompatActivity {
    // variables
    private TextView infoJeu;
    private EditText localisation;
    private Button valider;
    private Bundle extras;
    private String idJeu;
    private String urlPost;
    private String url;
    private String nouvelleLocalisation;
    private String jeuxLocation;
    private String game_id;
    private String userToken;

    // initialisation
    private void initialiser() {
        jeuxLocation = null;
        localisation = this.findViewById(R.id.localisation);
        infoJeu = this.findViewById(R.id.info_jeu);
        valider = this.findViewById(R.id.valider_retour);
        extras = getIntent().getExtras();
        idJeu = extras.getString("id_jeu");
        url =  BuildConfig.URL_KEY;
        extras = getIntent().getExtras();
        userToken = extras.getString("token");
    }

    // onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modiflieuid);

        initialiser();
        get_loan_info();

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                urlPost = url+"/games/"+game_id;
                nouvelleLocalisation = localisation.getText().toString();
                post(urlPost,nouvelleLocalisation);
                Intent intent = new Intent(ChoixModifEmplacementId.this, ChoixModifEmplacement.class);
                intent.putExtra("token" , userToken);
                startActivity(intent);
                finish();
            }
        });
    }

    //récupérer les info de l'emprunt
    private void get_loan_info(){
        try {
            JsonObject loan_info = Ion.with(findViewById(android.R.id.content).getRootView().getContext())
                    .load(url+"games/"+idJeu)
                    .asJsonObject()
                    .get();
            String game_name = loan_info.getAsJsonObject().getAsJsonObject().getAsJsonPrimitive("name").getAsString();
            game_id = loan_info.getAsJsonObject().getAsJsonObject().getAsJsonPrimitive("id").getAsString();
            infoJeu.setText("Nom du jeu : "+game_name+"\n Id du jeu : "+game_id);

            try {
                jeuxLocation = loan_info.getAsJsonObject().getAsJsonObject().getAsJsonPrimitive("location").getAsString();
                localisation.setText(jeuxLocation);
            }catch (Exception e){
                localisation.setText("");
                e.printStackTrace();
            }

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // methode post
    private void post(String url, String state){
        JsonObject json = new JsonObject();
        json.addProperty("location", state);

        // signale le retour du jeu
        Ion.with(getApplicationContext())
                .load(url)
                .setHeader("token",(userToken))
                .setJsonObjectBody(json)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (e != null) {
                            Toast.makeText(getBaseContext(), "Data : " + e.getStackTrace(), Toast.LENGTH_LONG).show();
                        } else if (result != null){
                            try {
                                String error = result.getAsJsonObject().getAsJsonPrimitive("raisonProvided").getAsString();
                                Toast.makeText(getBaseContext(), error, Toast.LENGTH_LONG).show();
                            }catch (Exception a){
                                a.printStackTrace();
                                Toast.makeText(getBaseContext(), "La localisation du jeu a été modifié !", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
    }

    // Clean de la spinnerList au retour de page
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(ChoixModifEmplacementId.this, ChoixModifEmplacement.class);
            intent.putExtra("token" , userToken);
            startActivity(intent);
            finish();
        }
        return true;
    }
}
